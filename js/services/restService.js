/**
 * Created by yagizozturk on 10/30/15.
 */
app.factory('restService', function () {
    // var context="http://localhost:9091/";
    var context="http://dev.soruavcisi.com/api/";
    //var context="http://www.soruavcisi.com/api/";
    return{
        getServiceUrl: function (service) {
            return context + this.serviceList[service];
        },
        getServiceUrlNC: function (service) {
            return context + this.serviceList[service] + '?_=' + new Date().getTime();
        },
        serviceList: {

            // Dashboard
            categoryStats  : "dashboard/categoryStats",
            categoryQuestionLevelStats  : "dashboard/categoryQuestionLevelStats",
            tagStats       : "dashboard/tagStats",
            levelStats     : "dashboard/questionLevelStats",
            userStats      : "dashboard/userStats",

            //Users
            allUsers       : "allUsers",
            singleUser     : "singleUser",
            updateUser     : "updateUser",

            // Questions
            allQuestions   : "allQuestions",
            singleQuestion : "singleQuestion",
            newQuestion    : "newQuestion",
            updateQuestion : "updateQuestion",
            uploadImage    : "uploadImage",

            // Categories
            allCategories  : "allCategories",
            singleCategory : "singleCategory",
            newCategory    : "newCategory",
            updateCategory : "updateCategory",

            // Tags
            allTags        : "allTags",
            singleTag      : "singleTag",
            newTag         : "newTag",
            updateTag      : "updateTag"
        }
    };
});