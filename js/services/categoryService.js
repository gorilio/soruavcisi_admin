/**
 * Created by yagizozturk on 10/30/15.
 */
app.factory('categoryService', function ($http, restService) {
    return{
        getAllCategories: function () {
            return $http.get(restService.getServiceUrl("allCategories"));
        },
        getSingleCategory: function (categoryId) {
            return $http.get(restService.getServiceUrl("singleCategory") + "/" + categoryId);
        },
        getStats: function () {
            return $http.get(restService.getServiceUrl("categoryStats"));
        },
        getQuestionLevelStats: function () {
            return $http.get(restService.getServiceUrl("categoryQuestionLevelStats"));
        },
        createNewCategory: function (category) {
            return $http.post(restService.getServiceUrl("newCategory"), category);
        },
        updateCategory: function (category) {
            return $http.post(restService.getServiceUrl("updateCategory") + "/" + category.id, category);
        }
    };
});