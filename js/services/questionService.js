/**
 * Created by yagizozturk on 10/30/15.
 */
app.factory('questionService', function ($http, restService) {
    return{
        getAllQuestions: function () {
            return $http.get(restService.getServiceUrl("allQuestions"));
        },
        getSingleQuestion: function (questionId) {
            return $http.get(restService.getServiceUrl("singleQuestion") + "/" + questionId);
        },
        getLevelStats: function () {
            return $http.get(restService.getServiceUrl("levelStats"));
        },
        createNewQuestion: function (question) {
            return $http.post(restService.getServiceUrl("newQuestion"), question);
        },
        updateQuestion: function (question) {
            return $http.post(restService.getServiceUrl("updateQuestion") + "/" + question.id, question);
        },
        uploadImage: function (file){
            var fd = new FormData();
            fd.append('file', file);
            return $http.post(restService.getServiceUrl("uploadImage"), fd, {transformRequest: angular.identity, headers: {'Content-Type': undefined}});
        }
    };
});