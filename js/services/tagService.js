/**
 * Created by yagizozturk on 10/30/15.
 */
app.factory('tagService', function ($http, restService) {
    return{
        getAllTags: function () {
            return $http.get(restService.getServiceUrl("allTags"));
        },
        getSingleTag: function (tagId) {
            return $http.get(restService.getServiceUrl("singleTag") + "/" + tagId);
        },
        getStats: function () {
            return $http.get(restService.getServiceUrl("tagStats"));
        },
        createNewTag: function (tag) {
            return $http.post(restService.getServiceUrl("newTag"), tag);
        },
        updateTag: function (tag) {
            return $http.post(restService.getServiceUrl("updateTag") + "/" + tag.id, tag);
        }
    };
});