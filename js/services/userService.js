/**
 * Created by yagizozturk on 11/26/15.
 */
app.factory('userService', function ($http, restService) {
    return{
        getAllUsers: function () {
            return $http.get(restService.getServiceUrl("allUsers"));
        },
        getSingleUser: function (userId) {
            return $http.get(restService.getServiceUrl("singleUser") + "/" + userId);
        },
        getStats: function () {
            return $http.get(restService.getServiceUrl("userStats"));
        },
        updateUser: function (user) {
            return $http.post(restService.getServiceUrl("updateUser") + "/" + user.id, user);
        }
    };
});
