app.factory('errorService', function() {
    return {
        getErrorDesc: function(msg){
            if(this.errors[msg]!=undefined)
                return this.errors[msg];

            return this.errors.DEFAULT;
        },
        errors:{
            DEFAULT:  "Bir hata oluştu",
            SUCCESS:  "Başarılı",
            MISSING_FIELD: "Lütfen alanları kontrol ediniz!"
        }
    };
});
