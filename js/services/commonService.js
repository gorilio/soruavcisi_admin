app.factory('commonService', function ($rootScope, toaster, errorService) {
    return{
        createEmptyUserObjectIfUndefined: function () {
            if ($rootScope.activeUser == undefined) {
                $rootScope.activeUser = {};
            }
        },
        deleteWhiteSpace: function (text) {
            text = text.replace(/ /g, '');
            return text;
        },
        replaceWhiteSpace: function (text) {
            text = text.replace(/ /g, '-');
            return text;
        },
        replaceTrChars: function (text) {
            text = text.replace(/ş/g, "s");
            text = text.replace(/ç/g, "c");
            text = text.replace(/ğ/g, "g");
            text = text.replace(/ö/g, "o");
            text = text.replace(/ü/g, "u");
            text = text.replace(/ı/g, "i");
            text = text.replace(/Ş/g, "s");
            text = text.replace(/Ç/g, "C");
            text = text.replace(/Ğ/g, "G");
            text = text.replace(/Ö/g, "O");
            text = text.replace(/Ü/g, "U");
            text = text.replace(/İ/g, "I");
            return text;
        },
        convertToUrl: function (text) {
            text = this.replaceTrChars(text);
            text = this.replaceWhiteSpace(text);
            return text;
        },
        findByIndex: function (text) {
            text = text.replace(/ /g, '-');
            return text;
        },
        compareRatio: function (text) {
            text = text.replace(/ /g, '-');
            return text;
        },
        compareRatioValue: function (text) {
            text = text.replace(/ /g, '-');
            return text;
        },
        arrayContains: function (array, value) {
            return!!~array.indexOf(value);
        },
        stringContains: function(original, search) {
            if (original.indexOf(search) > -1) {
                return true;
            } else {
                return false;
            }
        },
        isEmptyObject: function(o){
            for ( var p in o ) {
                if ( o.hasOwnProperty( p ) ) { return false; }
            }
            return true;
        },
        alertSuccess: function(title){
            toaster.pop({type: 'success', title: title, body: errorService.getErrorDesc("SUCCESS"), showCloseButton: true, timeout: 3000});
        },
        alertError: function(title, error){
            toaster.pop({type: 'error', title: title, body: 'Hata oluştu: ' + errorService.getErrorDesc(error), showCloseButton: true, timeout: 3000});
        }
    };
});