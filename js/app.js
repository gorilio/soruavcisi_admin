var app = angular.module('soruavcisi', ['ngRoute', 'ui.bootstrap', 'ngSanitize', 'ui.bootstrap', 'localytics.directives', 'datatables', 'toaster'])
    .config(['$routeProvider', '$httpProvider', function ($routeProvider) {
        $routeProvider
            .when('/ozet', {templateUrl: 'views/dashboard/dashboard.html?v=1.0.0', controller: dashboardCtrl})
            .when('/sorular', {templateUrl: 'views/question/all_questions.html?v=1.0.0', controller: questionCtrl})
            .when('/oyuncular', {templateUrl: 'views/user/all_users.html?v=1.0.0', controller: userCtrl})
            .when('/yeni-soru', {templateUrl: 'views/question/single_question.html?v=1.0.0', controller: questionCtrl})
            .when('/soru/:questionId', {templateUrl: 'views/question/single_question.html?v=1.0.0', controller: questionCtrl})
            .when('/kategoriler', {templateUrl: 'views/category/all_categories.html?v=1.0.0', controller: categoryCtrl})
            .when('/yeni-kategori', {templateUrl: 'views/category/single_category.html?v=1.0.0', controller: categoryCtrl})
            .when('/kategori/:categoryId', {templateUrl: 'views/category/single_category.html?v=1.0.0', controller: categoryCtrl})
            .when('/etiketler', {templateUrl: 'views/tag/all_tags.html?v=1.0.0', controller: tagCtrl})
            .when('/yeni-etiket', {templateUrl: 'views/tag/single_tag.html?v=1.0.0', controller: tagCtrl})
            .when('/etiket/:tagId', {templateUrl: 'views/tag/single_tag.html?v=1.0.0', controller: tagCtrl})
            .otherwise({redirectTo: '/ozet'});
    }]);

app.run(function ($rootScope, $location, $http) {
    $http.defaults.headers.common['Authorization'] = 'c183a5e7f17ad3311ca1eb61a6d995e38bbfe8bae5e03e08e2a5d55725a90123935d3526c39821a7b7f2fc16a20371f2e86c182a342dd8426c10525f6f2df6ca0bf2ceb447dd39d8ece69bac6f3cb6733a31bf15d567aab6644cf02f964352cf044e45be97ed7c428485c7b01914a65ed0b1bc9313f2b188f9046b1861fb2c27';
    $http.defaults.headers.common['Accept'] = 'application/json;odata=verbose';

    $rootScope.selectedWhen = function (value) {
        if ($location.url().indexOf(value) >= 0) {
            return 'active';
        }
    };
});