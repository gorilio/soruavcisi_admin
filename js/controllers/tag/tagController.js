function tagCtrl($scope, $routeParams, tagService, commonService) {

    var tagId;

    init();

    function init () {
        tagId = $routeParams.tagId;
        if(tagId != undefined) {
            getSingleTag(tagId);
        } else {
            getAllTags();
        }
    };

    function getAllTags () {
        tagService.getAllTags()
            .success(function (data) {
                $scope.tags = data;
            })
            .error(function (err) {
                commonService.alertError("Tüm Etiketler", err);
            });
    };

    function getSingleTag () {
        tagService.getSingleTag(tagId)
            .success(function (data) {
                $scope.t = data;
            })
            .error(function (err) {
                commonService.alertError("Etiket Güncelleme", err);
            });
    };

    $scope.applyTag = function() {

        // Validation
        if($scope.t == undefined || $scope.t.name == "") {
            commonService.alertError("Eksik Bilgi", "MISSING_FIELD");
            return;
        }

        if(tagId != undefined) {
            tagService.updateTag($scope.t)
                .success(function () {
                    commonService.alertSuccess("Etiket Güncelleme");
                })
                .error(function (err) {
                    commonService.alertError("Etiket Güncelleme", err);
                });
        } else {
            tagService.createNewTag($scope.t)
                .success(function () {
                    commonService.alertSuccess("Yeni Etiket");
                })
                .error(function (err) {
                    commonService.alertError("Yeni Etiket", err);
                });
        }
    };
}