function categoryCtrl($scope, $routeParams, categoryService, commonService) {

    var categoryId;

    init();

    function init () {
        categoryId = $routeParams.categoryId;
        if(categoryId != undefined) {
            getSingleCategory(categoryId);
        } else {
            getAllCategories();
        }
    };

    function getAllCategories () {
        categoryService.getAllCategories()
            .success(function (data) {
                $scope.categories = data;
            })
            .error(function (err) {
                commonService.alertError("Tüm Kategoriler", err);
            });
    };

    function getSingleCategory (categoryId) {
        categoryService.getSingleCategory(categoryId)
            .success(function (data) {
                $scope.c = data;
            })
            .error(function (err) {
                commonService.alertError("Kategori Güncelleme", err);
            });
    };

    $scope.applyCategory = function() {

        // Validation
        if($scope.c == undefined || $scope.c.name == "") {
            commonService.alertError("Eksik Bilgi", "MISSING_FIELD");
            return;
        }

        if(categoryId != undefined) {
            categoryService.updateCategory($scope.c)
                .success(function () {
                    commonService.alertSuccess("Kategori Güncelleme");
                })
                .error(function (err) {
                    commonService.alertError("Kategori Güncelleme", err);
                });
        } else {
            categoryService.createNewCategory($scope.c)
                .success(function () {
                    commonService.alertSuccess("Yeni Kategori");
                })
                .error(function (err) {
                    commonService.alertError("Yeni Kategori", err);
                });
        }
    };
}