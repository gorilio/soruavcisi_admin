function userCtrl($scope, $routeParams, userService, commonService) {

    var userId;

    init();

    function init () {
        userId = $routeParams.userId;
        if(userId != undefined) {
            getSingleUser(userId);
        } else {
            getAllUsers();
        }
    };

    function getAllUsers () {
        userService.getAllUsers()
            .success(function (data) {
                $scope.users = data;
            })
            .error(function (err) {
                commonService.alertError("Tüm Oyuncular", err);
            });
    };

    function getSingleUser (userId) {
        userService.getSingleUser(userId)
            .success(function (data) {
                $scope.u = data;
            })
            .error(function (err) {
                commonService.alertError("Oyuncu Güncelleme", err);
            });
    };

    $scope.applyUser = function() {

        // Validation
        if($scope.u == undefined || $scope.u.name == "") {
            commonService.alertError("Eksik Bilgi", "MISSING_FIELD");
            return;
        }

        userService.updateUser($scope.u)
            .success(function () {
                commonService.alertSuccess("Oyuncu Güncelleme");
            })
            .error(function (err) {
                commonService.alertError("Oyuncu Güncelleme", err);
            });
    };
}