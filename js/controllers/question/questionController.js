/**
 * Created by yagizozturk on 11/17/15.
 */
function questionCtrl($scope, $routeParams, $q, questionService, categoryService, tagService, commonService) {

    var questionId;
    var _selectedCategories = [];
    var _selectedTags = [];

    init();

    function init() {
        questionId = $routeParams.questionId;

        var defer = $q.defer();

        if (questionId != undefined) {
            getAllCategories()
                .then(getAllTags)
                .then(getSingleQuestion);
        } else {
            getAllCategories()
                .then(getAllTags)
                .then(getAllQuestions);
        }

        defer.resolve();
    };

    function getAllCategories() {
        return categoryService.getAllCategories()
            .success(function (data) {
                $scope.categories = data;
            })
            .error(function (err) {
                commonService.alertError("Tüm Kategoriler", err);
            });
    };

    function getAllTags() {
        return tagService.getAllTags()
            .success(function (data) {
                $scope.tags = data;
            })
            .error(function (err) {
                commonService.alertError("Tüm Etiketler", err);
            });
    };

    function getAllQuestions() {
        return questionService.getAllQuestions()
            .success(function (data) {
                $scope.questions = data;
            })
            .error(function (err) {
                commonService.alertError("Tüm Sorular", err);
            });
    };

    function getSingleQuestion() {
        questionId = $routeParams.questionId;
        return questionService.getSingleQuestion(questionId)
            .success(function (data) {
                $scope.q = data;

                $scope.selectedCategories = [];
                $scope.selectedTags = [];

                // Iki defa donmemeizin sebebi selectedCategories in obje array i aliyor olmasi, Esas cekilen categories objesi ile eslestirip icindeki 'name' field larini da aliyoruz. Tag icinde ayni sey gecerli.
                angular.forEach($scope.categories, function (category) {
                    angular.forEach($scope.q.categories, function (categorySelected) {
                        if (categorySelected.id == category.id) {
                            $scope.selectedCategories.push(category);
                        }
                    });
                });

                angular.forEach($scope.tags, function (tag) {
                    angular.forEach($scope.q.tags, function (tagSelected) {
                        if (tagSelected.id == tag.id) {
                            $scope.selectedTags.push(tag);
                        }
                    });
                });

                angular.forEach($scope.q.answers, function (answer, index) {
                    if (index + 1 == 1) {
                        $scope.answerOne = answer.answer;
                    } else if (index + 1 == 2) {
                        $scope.answerTwo = answer.answer;
                    } else {
                        $scope.answerThree = answer.answer;
                    }

                    // Dogru cevabi bulup onu radio button a atiyoruz
                    if (answer.is_correct == true) {
                        $scope.isCorrectAnswer = index + 1;
                    }
                });
            })
            .error(function (err) {
                commonService.alertError("Tek Soru", err);
            });

    };

    $scope.$watch("selectedCategories", function (newValue) {
        _selectedCategories = [];
        angular.forEach(newValue, function (data) {
            _selectedCategories.push(data.id);
        });
    });

    $scope.$watch("selectedTags", function (newValue) {
        _selectedTags = [];
        angular.forEach(newValue, function (data) {
            _selectedTags.push(data.id);
        });
    });

    $scope.uploadImage = function () {
        var file = $scope.myFile;
        questionService.uploadImage(file)
            .success(function (response) {
                commonService.alertSuccess("Imaj yüklendi!");
                $scope.q.image = response.filename;
            })
            .error(function (err) {
                commonService.alertError("Imaj yüklerken hata oluştu", err);
            });
    };

    $scope.imageChanged = function (input) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $scope.uploadedImageSrc = e.target.result;
        $scope.$apply();
      };
      reader.readAsDataURL(input.files[0]);
    };

    $scope.applyQuestion = function () {

        // Validation
        if ($scope.q == undefined || $scope.q.question == "" || $scope.q.points == undefined
            || _selectedCategories.length == 0 || $scope.answerOne == ""
            || $scope.answerTwo == "" || $scope.answerThree == "" || $scope.isCorrectAnswer == undefined) {
            commonService.alertError("Eksik Bilgi", "MISSING_FIELD");
            return;
        }

        $scope.q.categories = _selectedCategories;
        $scope.q.tags = _selectedTags;
        $scope.q.points = parseInt($scope.q.points);

        switch ($scope.q.points) {
            case 1:
                $scope.q.level = 10;
                break;
            case 2:
                $scope.q.level = 30;
                break;
            case 3:
                $scope.q.level = 50;
                break;
            case 4:
                $scope.q.level = 70;
                break;
            case 5:
                $scope.q.level = 90;
                break;
        }

        var isCorrectAnswerOne = false;
        var isCorrectAnswerTwo = false;
        var isCorrectAnswerThree = false;

        switch (parseInt($scope.isCorrectAnswer)) {
            case 1:
                isCorrectAnswerOne = true;
                break;
            case 2:
                isCorrectAnswerTwo = true;
                break;
            case 3:
                isCorrectAnswerThree = true;
                break;
        }

        if (questionId != undefined) {
            $scope.q.answers[0] = {
                id: $scope.q.answers[0].id,
                answer: $scope.answerOne,
                is_correct: isCorrectAnswerOne
            };
            $scope.q.answers[1] = {
                id: $scope.q.answers[1].id,
                answer: $scope.answerTwo,
                is_correct: isCorrectAnswerTwo
            };
            $scope.q.answers[2] = {
                id: $scope.q.answers[2].id,
                answer: $scope.answerThree,
                is_correct: isCorrectAnswerThree
            };

            questionService.updateQuestion($scope.q)
                .success(function () {
                    commonService.alertSuccess("Soru Güncelleme");
                })
                .error(function (err) {
                    commonService.alertError("Soru Güncelleme", err);
                });
        } else {
            $scope.q.answers = []

            var answers = [];
            answers.push({answer: $scope.answerOne, is_correct: isCorrectAnswerOne});
            answers.push({answer: $scope.answerTwo, is_correct: isCorrectAnswerTwo});
            answers.push({answer: $scope.answerThree, is_correct: isCorrectAnswerThree});

            $scope.q.answers = answers;

            questionService.createNewQuestion($scope.q)
                .success(function () {
                    clearFormElements();
                    commonService.alertSuccess("Yeni Soru");
                })
                .error(function (err) {
                    commonService.alertError("Yeni Soru", err);
                });
        }
    };

    function clearFormElements() {
        $scope.q = null;
        $scope.isCorrectAnswer = null;
        $scope.answerOne = null;
        $scope.answerTwo = null;
        $scope.answerThree = null;
        $scope.selectedCategories = [];
        $scope.selectedTags = [];
    };
}