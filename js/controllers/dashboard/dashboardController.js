function dashboardCtrl($scope, questionService, categoryService, tagService, commonService){

    init();

    function init () {
        getCategoryStats();
        getCategoryQuestionCountStats();
        getTagStats();
        getQuestionLevelStats()
    };

    function getCategoryStats () {
        categoryService.getStats()
            .success(function (data) {
                $scope.categoryStats = data;
            })
            .error(function (err) {
                commonService.alertError("Özet Kategoriler", err);
            });
    };

    function getCategoryQuestionCountStats () {
        categoryService.getQuestionLevelStats()
            .success(function (data) {
                $scope.categoryQuestionCount = data;
            })
            .error(function (err) {
                commonService.alertError("Özet Kateogori Soru Sayilari", err);
            });
    };

    function getTagStats () {
        tagService.getStats()
            .success(function (data) {
                $scope.tagStats = data;
            })
            .error(function (err) {
                commonService.alertError("Özet Etiketler", err);
            });
    };

    function getQuestionLevelStats () {
        questionService.getLevelStats()
            .success(function (data) {
                $scope.levelStats = data;
            })
            .error(function (err) {
                commonService.alertError("Özet Zorluklar", err);
            });
    };
}